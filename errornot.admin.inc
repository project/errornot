<?php

/**
 * Form callback: configuration form for ErrorNot.
 */
function errornot_configuration_form($form, &$form_state) {
  $form['errornot_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#default_value' => '',
    '#description' => t('The full URL to the ErrorNot server, in the form @url.', array('@url' => 'http://errornot.my.com/errors/')),
  );

  $form['errornot_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => '',
    '#description' => t('The API key of your websites on the ErrorNot server.'),
  );

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => 'Message filters',
    '#description' => 'Watchdog messages will be sent to ErrorNot if they match all those conditions.',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['filter']['errornot_minimum_severity'] = array(
    '#type' => 'select',
    '#title' => t('Minimum severity'),
    '#options' => watchdog_severity_levels(),
    '#default_value' => WATCHDOG_ERROR,
  );

  return system_settings_form($form);
}
